﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AnimalsMVC.Infrastructure;
using AnimalsMVC.Models;

namespace AnimalsMVC.Controllers
{
    public class ZooController : Controller
    {
        private AnimalsRepository _repo;

        public ZooController()
        {
            _repo = new AnimalsRepository(new ZooContext());
        }

        /// <summary>
        /// Главная страница с каталогом.
        /// </summary>
        /// <returns></returns>
        public ActionResult Catalog()
        {
            return View();
        }

        /// <summary>
        /// Страница добавления новой записи.
        /// </summary>
        /// <returns></returns>
        public ActionResult AddAnimal()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddAnimal(Animal model)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("msg-error", "Model validation error.");
                return View();
            }

            try
            {
                var item = _repo.AddAnimal(model);
            }
            catch (Exception)
            {
                ModelState.AddModelError("msg-error", "Operation error. Please, try again later.");
                return View();
            }

            return RedirectToAction("Catalog");
        }

        /// <summary>
        /// Получение записи о животном.
        /// </summary>
        /// <param name="animalId">ID записи.</param>
        /// <returns></returns>
        public PartialViewResult GetAnimalAjax(long animalId)
        {
            var record = _repo.GetAnimal(animalId);

            return PartialView("_UpdateAnimal", record);
        }
        
        /// <summary>
        /// Обновить содержимое записи.
        /// </summary>
        /// <param name="model">Объект обновления.</param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult UpdateAnimal(Animal model)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("msg-error", "Model validation error.");
            }

            try
            {
                _repo.UpdateAnimal(model);
                ModelState.AddModelError("msg-success", "Updated successfully.");
            }
            catch (Exception)
            {
                ModelState.AddModelError("msg-error", "Model updating error.");
            }

            return PartialView("_UpdateAnimal");
        }

        /// <summary>
        /// Загрузка данных в таблицу.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public JsonResult LoadTableContentAjax()
        {
            if (!Request.IsAjaxRequest())
                return Json("request_type_error", JsonRequestBehavior.AllowGet);
            
            // построить сигнатуру
            var tableRequestData = AnimalsTableHandler.ExtractData(Request);

            // запросить данные по сигнатуре
            var found = _repo.GetAnimals(
                tableRequestData.OrderByNameAsc,
                tableRequestData.TypeFilter,
                tableRequestData.ColorFilter,
                tableRequestData.RegionFilter
                );

            // число выбранных/пропущенных записей
            int takeCount = tableRequestData.Length;
            int skipCount = tableRequestData.Start;

            // форматировать данные для вывода в js-таблице
            var foundFormatted = found.Select(
                x => new
                {
                    id = x.Id,
                    name = x.Name,
                    type = x.Type.Name,
                    color = x.Color.Name,
                    location = x.Location.Name,
                    region = x.Location.Region.Name,
                }).Skip(skipCount).Take(takeCount).ToArray();

            // сформировать ответную сигнатуру
            var tableResponseData = new {
                data = foundFormatted,
                draw = tableRequestData.Draw,
                start = tableRequestData.Start,
                length = tableRequestData.Length,
                recordsTotal = _repo.GetAnimals().Count(),
                recordsFiltered = found.Count()
            };

            return Json(tableResponseData, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Удаление выбранной записи.
        /// </summary>
        /// <param name="animalId">ID записи.</param>
        /// <returns></returns>
        public ActionResult RemoveAnimalAjax(long animalId)
        {
            if (animalId == 0)
                return Json(false, JsonRequestBehavior.AllowGet);

            try
            {
                _repo.RemoveAnimal(animalId);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
    }
}