﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using AnimalsMVC.Models;

namespace AnimalsMVC.Infrastructure
{
    /// <summary>
    /// Класс-инициализатор для создания тестовой БД с данными.
    /// </summary>
    public class SampleDbInitializer : CreateDatabaseIfNotExists<ZooContext> 
    {
        public override void InitializeDatabase(ZooContext context)
        {
            base.InitializeDatabase(context);
        }

        /// <summary>
        /// Занести в БД тестовый набор данных.
        /// </summary>
        /// <param name="context">Объект ZooContext.</param>
        protected override void Seed(ZooContext context)
        {
            #region Set animal types types
            AnimalType[] types = new AnimalType[]
            {
                new AnimalType { Name = "Mammals" },
                new AnimalType { Name = "Reptiles" },
                new AnimalType { Name = "Birds" },
                new AnimalType { Name = "Amphibians" },
                new AnimalType { Name = "Fishes" }
            };

            context.AnimalTypes.AddRange(types);
            context.SaveChanges();
            #endregion

            #region Set skin colors
            SkinColor[] colors = new SkinColor[]
            {
                new SkinColor { Name = "Mixed" },
                new SkinColor { Name = "Brown" },
                new SkinColor { Name = "Sand" },
                new SkinColor { Name = "Red" },
                new SkinColor { Name = "Black" },
                new SkinColor { Name = "Grey" },
                new SkinColor { Name = "Green" },
                new SkinColor { Name = "White" }
            };

            context.Colors.AddRange(colors);
            context.SaveChanges();
            #endregion

            #region Set regions
            Region[] regions = new Region[]
            {
                new Region { Name = "Africa" },
                new Region { Name = "North America" },
                new Region { Name = "South America" },
                new Region { Name = "Europe" },
                new Region { Name = "Asia" }
            };

            context.Regions.AddRange(regions);
            context.SaveChanges();
            #endregion

            #region Set locations
            Location[] locations = new Location[]
            {
                new Location { Name = "Kenya", RegionId = 1 },
                new Location { Name = "Argentina", RegionId = 3 },
                new Location { Name = "Canada", RegionId = 2 },
                new Location { Name = "China", RegionId = 5 }
            };

            context.Locations.AddRange(locations);
            context.SaveChanges();
            #endregion

            #region Set some animals
            Animal[] animals = new Animal[]
            {
                new Animal { Name = "Zebra", ColorId = 1, TypeId = 1, LocationId = 1 },
                new Animal { Name = "Lion", ColorId = 3, TypeId = 1, LocationId = 1 },
                new Animal { Name = "Crocodile", ColorId = 7, TypeId = 2, LocationId = 1 },
                new Animal { Name = "Puma", ColorId = 5, TypeId = 1, LocationId = 2 },
                new Animal { Name = "Caiman", ColorId = 6, TypeId = 2, LocationId = 2 },
                new Animal { Name = "Condor", ColorId = 1, TypeId = 3, LocationId = 2 },
                new Animal { Name = "Atlantic salmon", ColorId = 6, TypeId = 5, LocationId = 3 },
                new Animal { Name = "Tailed frog", ColorId = 7, TypeId = 4, LocationId = 3 },
                new Animal { Name = "Snowy owl", ColorId = 8, TypeId = 3, LocationId = 3 },
                new Animal { Name = "Dice snake", ColorId = 7, TypeId = 2, LocationId = 4 },
                new Animal { Name = "White dolphin", ColorId = 8, TypeId = 1, LocationId = 4 },
                new Animal { Name = "Vernal parrot", ColorId = 7, TypeId = 3, LocationId = 4 }
            };

            context.Animals.AddRange(animals);
            context.SaveChanges();
            #endregion
        }
    }
}