﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AnimalsMVC.Models;
using AnimalsMVC.Infrastructure;

namespace AnimalsMVC.Infrastructure
{
    /// <summary>
    /// Класс-справочник для получения информации о животных.
    /// </summary>
    public static class AnimalDetailsHelper
    {
        private static AnimalsRepository _repo;

        static AnimalDetailsHelper()
        {
            _repo = new AnimalsRepository(new ZooContext());
        }

        /// <summary>
        /// Получить справочник типов животных.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<AnimalType> GetAnimalTypes()
        {
            return _repo.GetTypes();
        }

        /// <summary>
        /// Получить справочник цветов шкур животных.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<SkinColor> GetSkinColors()
        {
            return _repo.GetColors();
        }

        /// <summary>
        /// Получить справочник мест обитаний животных.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Location> GetAnimalLocations()
        {
            return _repo.GetLocations();
        }
    }
}