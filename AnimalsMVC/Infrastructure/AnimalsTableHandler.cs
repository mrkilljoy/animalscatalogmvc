﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace AnimalsMVC.Infrastructure
{
    /// <summary>
    /// Класс для извлечения данных из запроса, приходящего от таблицы.
    /// </summary>
    public static class AnimalsTableHandler
    {
        /// <summary>
        /// Извлечь данные из запроса и построить его сигнатуру.
        /// </summary>
        /// <param name="request">Объект запроса.</param>
        /// <returns></returns>
        public static TableRequest ExtractData(HttpRequestBase request)
        {
            string draw = request.QueryString["draw"];
            string start = request.QueryString["start"];
            string length = request.QueryString["length"];
            
            string typeFilter = request.QueryString["columns[2][search][value]"];
            string colorFilter = request.QueryString["columns[3][search][value]"];
            string regionFilters = request.QueryString["columns[4][search][value]"];
            
            string orderType = request.QueryString["order[0][dir]"];

            var result = new TableRequest
            {
                Draw = string.IsNullOrWhiteSpace(draw) ? 0 : Convert.ToInt32(draw),
                Start = string.IsNullOrWhiteSpace(start) ? 0 : Convert.ToInt32(start),
                Length = string.IsNullOrWhiteSpace(length) ? 0 : Convert.ToInt32(length),
                OrderByNameAsc = orderType == "asc" ? true : false,
                TypeFilter = string.IsNullOrWhiteSpace(typeFilter) ? "" : typeFilter.Replace("^(", "").Replace(")$", ""),
                ColorFilter = string.IsNullOrWhiteSpace(colorFilter) ? "" : colorFilter.Replace("^(", "").Replace(")$", ""),
                RegionFilter = string.IsNullOrWhiteSpace(regionFilters) ?
                new string[0] :
                regionFilters.Replace("^(", "").Replace(")$", "").Replace("\\", "").Split(new[] { '|' })
            };

            return result;
        }
    }

    /// <summary>
    /// Класс, описывающий сигнатуру запроса, приходящего от js-объекта DataTable.
    /// </summary>
    public class TableRequest
    {
        public int Draw { get; set; }

        /// <summary>
        /// Число пропускаемых с начала записей.
        /// </summary>
        public int Start { get; set; }

        /// <summary>
        /// Число выбираемых записей.
        /// </summary>
        public int Length { get; set; }

        /// <summary>
        /// Сортировка по имени (true - ASC, false - DESC).
        /// </summary>
        public bool OrderByNameAsc { get; set; }

        /// <summary>
        /// Значение для фильтрации по типу животного.
        /// </summary>
        public string TypeFilter { get; set; }

        /// <summary>
        /// Значение для фильтрации по цвету шкуры.
        /// </summary>
        public string ColorFilter { get; set; }

        /// <summary>
        /// Значение для фильтрации по регионам.
        /// </summary>
        public IEnumerable<string> RegionFilter { get; set; }
    }
}