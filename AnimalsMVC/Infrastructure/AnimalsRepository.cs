﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using AnimalsMVC.Models;

namespace AnimalsMVC.Infrastructure
{
    /// <summary>
    /// Класс-репозиторий для работы с записями о животных.
    /// </summary>
    public class AnimalsRepository : IDisposable
    {
        private ZooContext _context;

        public AnimalsRepository(ZooContext context)
        {
            _context = context;
        }

        public void Dispose()
        {
            ((IDisposable)_context).Dispose();
        }

        #region Methods

        /// <summary>
        /// Добавить запись.
        /// </summary>
        /// <param name="animal">Объект добавления.</param>
        /// <returns></returns>
        public Animal AddAnimal(Animal animal)
        {
            if (_context.Animals.Any(a => a.Name == animal.Name && a.TypeId == animal.TypeId && a.ColorId == a.ColorId))
                throw new Exception("Same record is already exists!");

            _context.Animals.Add(animal);
            _context.SaveChanges();

            return animal;
        }

        /// <summary>
        /// Получить запись.
        /// </summary>
        /// <param name="animalId">ID записи.</param>
        /// <returns></returns>
        public Animal GetAnimal(long animalId)
        {
            return _context.Animals.FirstOrDefault(a => a.Id == animalId);
        }

        /// <summary>
        /// Получить набор для выполнения выборки записей.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Animal> GetAnimals()
        {
            return _context.Animals.Include(a => a.Type)
                .Include(a => a.Color)
                .Include(a => a.Location)
                .Include(a => a.Location.Region);
        }

        /// <summary>
        /// Получить группу записей.
        /// </summary>
        /// <param name="sortNameByAsc">Сортировка по имени (true - по возрастанию, false - по убыванию).</param>
        /// <param name="type">Тип животного.</param>
        /// <param name="color">Цвет шкуры.</param>
        /// <param name="regions">Регион</param>
        /// <returns></returns>
        public IQueryable<Animal> GetAnimals(bool sortNameByAsc, string type, string color, IEnumerable<string> regions)
        {
            var found = _context.Animals.AsQueryable();

            if (!string.IsNullOrWhiteSpace(type))
                found = found.Where(a => a.Type.Name == type);

            if (regions != null && regions.Any())
                found = from f in found.Include(a => a.Location).Include(a => a.Location.Region)
                        join r in regions on f.Location.Region.Name equals r
                        select f;

            if (!string.IsNullOrWhiteSpace(color))
                found = found.Where(a => a.Color.Name == color);

            if (sortNameByAsc)
                found = found.OrderBy(a => a.Name);
            else
                found = found.OrderByDescending(a => a.Name);

            return found.AsQueryable();
        }

        public IEnumerable<Animal> GetAnimals(bool sortNameByAsc, string type, string color, IEnumerable<string> regions, int takeCount = 10, int skipCount = 0)
        {
            var found = _context.Animals.AsQueryable();

            if (!string.IsNullOrWhiteSpace(type))
                found = found.Where(a => a.Type.Name == type);

            if (regions != null && regions.Any())
                found = from f in found.Include(a => a.Location).Include(a => a.Location.Region)
                        join r in regions on f.Location.Region.Name equals r
                        select f;

            if (!string.IsNullOrWhiteSpace(color))
                found = found.Where(a => a.Color.Name == color);

            if (sortNameByAsc)
                found = found.OrderBy(a => a.Name).Skip(skipCount).Take(takeCount);
            else
                found = found.OrderByDescending(a => a.Name).Skip(skipCount).Take(takeCount);

            return found.ToArray();
        }

        /// <summary>
        /// Получить список типов животных.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<AnimalType> GetTypes()
        {
            return _context.AnimalTypes.ToArray();
        }

        /// <summary>
        /// Получить список цветов шкур.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SkinColor> GetColors()
        {
            return _context.Colors.ToArray();
        }

        /// <summary>
        /// Получить список областей обитания.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Location> GetLocations()
        {
            return _context.Locations.ToArray();
        }

        /// <summary>
        /// Удалить запись.
        /// </summary>
        /// <param name="id">ID записи.</param>
        public void RemoveAnimal(long id)
        {
            var found = _context.Animals.FirstOrDefault(a => a.Id == id);
            if (found == null)
                return;

            _context.Entry(found).State = EntityState.Deleted;
            _context.SaveChanges();
        }

        /// <summary>
        /// Обновить запись.
        /// </summary>
        /// <param name="animal">Объект обновления.</param>
        /// <returns></returns>
        public bool UpdateAnimal(Animal animal)
        {
            _context.Entry(animal).State = EntityState.Modified;
            _context.SaveChanges();

            return true;
        }
        #endregion
    }
}