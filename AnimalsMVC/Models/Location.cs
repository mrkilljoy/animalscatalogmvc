﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnimalsMVC.Models
{
    /// <summary>
    /// Класс для описания области обитания.
    /// </summary>
    public class Location
    {
        [Key]
        public long Id { get; set; }

        public string Name { get; set; }

        public long RegionId { get; set; }

        [ForeignKey("RegionId")]
        public virtual Region Region { get; set; }
    }
}