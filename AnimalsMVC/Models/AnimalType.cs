﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace AnimalsMVC.Models
{
    /// <summary>
    /// Класс для описания типа животного.
    /// </summary>
    public class AnimalType
    {
        [Key]
        public long Id { get; set; }

        public string Name { get; set; }
    }
}