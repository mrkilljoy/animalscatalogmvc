namespace AnimalsMVC.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    /// <summary>
    /// �������� ������ ��� ������ � ��.
    /// </summary>
    public class ZooContext : DbContext
    {
        public ZooContext()
            : base("name=ZooContext")
        {
            Database.SetInitializer(new Infrastructure.SampleDbInitializer());
        }

        #region Collections
        public virtual DbSet<Animal> Animals { get; set; }

        public virtual DbSet<AnimalType> AnimalTypes { get; set; }

        public virtual DbSet<SkinColor> Colors { get; set; }

        public virtual DbSet<Location> Locations { get; set; }

        public virtual DbSet<Region> Regions { get; set; }

        #endregion
    }
}