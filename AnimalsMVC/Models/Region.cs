﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace AnimalsMVC.Models
{
    /// <summary>
    /// Класс для описания региона.
    /// </summary>
    public class Region
    {
        [Key]
        public long Id { get; set; }

        public string Name { get; set; }
    }
}