﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnimalsMVC.Models
{
    /// <summary>
    /// Класс для описания животного.
    /// </summary>
    public class Animal
    {
        [Key]
        public long Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public long TypeId { get; set; }

        [Required]
        public long ColorId { get; set; }

        [Required]
        public long LocationId { get; set; }

        [ForeignKey("TypeId")]
        public virtual AnimalType Type { get; set; }

        [ForeignKey("ColorId")]
        public virtual SkinColor Color { get; set; }

        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }
    }
}