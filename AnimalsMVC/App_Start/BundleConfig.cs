﻿using System.Web;
using System.Web.Optimization;

namespace AnimalsMVC
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region Scripts
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/jquery-uo").Include(
                        "~/Scripts/jquery.unobtrusive-ajax.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new ScriptBundle("~/bundles/dt").Include(
                      "~/Scripts/datatables.js",
                      "~/Scripts/dataTables*",
                      "~/Scripts/jquery.dataTables.js"));

            bundles.Add(new ScriptBundle("~/bundles/chosen").Include(
                      "~/Scripts/chosen.jquery.js"));

            bundles.Add(new ScriptBundle("~/bundles/dt-filters").Include(
                      "~/Scripts/ColumnFilterWidgets.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/custom").Include(
                      "~/Scripts/custom.js"));
            #endregion

            #region Styles
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/dt-css").Include(
                      "~/Content/datatables.css",
                      "~/Content/dataTables*",
                      "~/Content/chosen.css",
                      "~/Content/jquery.dataTables.css",
                      "~/Content/ColumnFilterWidgets.css"
                      ));
            #endregion
        }
    }
}
