﻿$(document).ready(function () {
    TableInitialize();

});

var table = null;

// Инициализация таблицы с данными
function TableInitialize() {
    table = $('#dt-animals').DataTable({
        ajax: {
            url: '/Zoo/LoadTableContentAjax',
        },
        serverSide: true,
        processing: true,
        paging: true,
        columns: [ // настройка столбцов таблицы
            {data: 'id', visible: false, searchable: false, orderable: false},
            { title: 'Наименование', data: 'name' },
            { title: 'Тип', data: 'type', searchable: true, orderable: false },
            { title: 'Цвет шкуры', data: 'color', searchable: true, orderable: false },
            { title: 'Регион', data: 'region', searchable: true, orderable: false },
            {
                searchable: false,
                orderable: false,
                width: '100px',
                data: null,
                render: function (data, type, full, meta) { // добавление кнопок в столбец
                    // кнопка просмотра записи
                    markup = '<a id="view-animal-' + meta.row + '" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-show-animal" onclick="DisplayAnimalModal(' + meta.row + ')"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a>';

                    // кнопка удаления записи
                    markup = markup.concat('<a id="drop-animal-' + meta.row + '" class="btn btn-sm btn-danger" onclick="RemoveTableRecord(' + data.id + ')"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>');

                    // кнопка изменения записи
                    markup = markup.concat('<a id="update-animal-' + meta.row + '" href="/Zoo/GetAnimalAjax?animalId=' + data.id + '" class="btn btn-sm btn-default" data-ajax="true" data-ajax-method="GET" data-ajax-mode="replace" data-ajax-update="#hex" data-ajax-complete="ShowEditAnimalModal"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span></a>');

                    return markup;
                }
            }

        ],
        language: {
            zeroRecords: 'Нет данных.',
            lengthMenu: 'Показывать по _MENU_ записей',
            info: "Показаны записи с _START_ по _END_, всего _TOTAL_ записей",
            infoFiltered: "(выбрано из _MAX_ записей)",
            infoEmpty: "Показано 0 из 0 записей.",
            loadingRecords: 'Загрузка...',
            processing: 'Подождите...',
            search: 'Поиск ',
            paginate: {
                next: 'Следующий',
                previous: 'Предыдущий'
            }
        },

        // компонент ColumnFilterWidgets (настройка multi-select фильтрации)
        sDom: 'Wlriptip',
        oColumnFilterWidgets: {
            aiExclude: [0, 1, 5],
            bGroupTerms: false,
            aoColumnDefs: [
                { bSort: false, iMaxSelections: 1, aiTargets: [2] },
                { bSort: false, iMaxSelections: 1, aiTargets: [3] },
                { bSort: false, aiTargets: [4] }
            ]

        }
    });
}

// Заполнение модального окна с информацией о выбранной записи
function DisplayAnimalModal(rowId) {
    if (!rowId || rowId == 0)
        return;

    let rowData = table.data()[rowId];

    $('#info-animal-name').val(rowData.name);
    $('#info-animal-type').val(rowData.type);
    $('#info-animal-color').val(rowData.color);
    $('#info-animal-location').val(rowData.location);

}

// Запрос на удаление записи
function RemoveTableRecord(id) {
    if (!id)
        return;

    $.get({
        url: '/Zoo/RemoveAnimalAjax',
        data: { animalId: id }
    }).then(function (data) {
        table.ajax.reload(null, true);
    });
}

// Вывод модального окна для редактирования записи
function ShowEditAnimalModal() {
    $('#modal-edit-animal').modal('show');
}

// Обновление содержимого таблицы после редактирования
function OnEditAnimalComplete() {
    table.ajax.reload(null, true);
}